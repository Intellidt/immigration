<?php include "inc/header.php" ?>

       <section class="contact-back">
           <div class="banner-back">
           </div>
            <div class="container">
               <div class="row">
                  <div class="col-md-12">
                     <div class="title-text">
                        <h2>Risk Free Consultation Services</h2>
                     </div>
                  </div>
               </div>
            </div>
         </section>
 <section class="contact-section contact-form-back quick-contact-form">
     <div class="container">
         <div class="row">
             <div class="col-md-6">
                 <div class="contact-form-section ">
                     <h5 class="">Feel free to get in touch with us</h5>
                     <p>At Intelli Management Consulting, we value our client’s confidentiality. In order to provide you with an accurate assessment of your options, please provide your contact information and one of our consulting experts will get in touch with you.</p>
                 </div>
                 <div class="contact-form-section mt-5">
                     <h5 class="">Contact Information</h5>
                     <div class="contact-table-wrap d-flex">
                         <div class="table-item">Address</div>
                         <div class="table-item-right">130-7080 River Road, Richmond, BC, V6X 1X5 Canada</div>
                     </div>
                     <div class="contact-table-wrap d-flex">
                         <div class="table-item">Phone </div>
                         <div class="table-item-right">1-778-297-7108</div>
                     </div>
                     <div class="contact-table-wrap d-flex">
                         <div class="table-item">Email</div>
                         <div class="table-item-right">contact@intelliconsultation.com</div>
                     </div>
                     <div class="contact-table-wrap d-flex">
                         <div class="table-item">Opening</div>
                         <div class="table-item-right">Monday to Friday : 10:00am to 5:00 pm (PST)</div>
                     </div>
                 </div>
             </div>
             <div class="col-md-6">
                 <div class="w-75 mx-auto contact-form-section">
                     <h5 class="mb-3">Free Consultation and Assessment</h5>
                     <p class="mb-4">Fill out our Assessment Form and we’ll review your eligibility for the immigration programs.</p>

                     <form action="" method="post">
                         <div class="row ">
                             <div class="col-md-12"><input  class="form-input" placeholder="First Name" name="fname" type="text" required></div>
                         </div>
                         <div class="row mt-20 ">
                             <div class="col-md-12"><input  class="form-input" placeholder="Last Name" name="lname" type="text" required></div>
                         </div>
                         <div class="row mt-20 ">
                             <div class="col-md-12"><input class="form-input" placeholder="Email"  name="email" type="email" required></div>
                         </div>
                         <div class="row mt-20 ">
                             <div class="col-md-12"><input class="form-input" placeholder="Telephone" name="phone" type="text" required></div>
                         </div>
                         <div class="row mt-20 ">
                             <div class="col-md-12"><input class="form-input" placeholder="Country of Residence" name="country" type="text" required></div>
                         </div>
                         <div class="row mt-20 ">
                             <div class="col-md-12 mx-auto text-center">
                                 <textarea name="contact-text" cols="60" rows="4" placeholder="How can we help you? Be as descriptive as possible." name="message" required></textarea>
                                 <input class="comment-button float-right" value="Send Message" name="send" type="submit">
                             </div>
                         </div>
                     </form>
                 </div>
             </div>
         </div>
     </div>
</section>


<?php
if(isset($_REQUEST['send'])) {
    $subject = "Website Inquiry";
    $message = $_POST['contact-text'];
    $fname=$_POST['fname'];
    $lname=$_POST['lname'];
    $phone=$_POST['phone'];
    $email2=$_POST['email'];
    $country = $_POST['country'];
    $email = "contact@intelliconsultation.com";
    $headers = 'From:' . $email2 . "\r\n"; // Sender's Email
    $message = 'First Name: '. $fname."\r\n".'Last Name: '. $lname."\r\n".'Email: '. $email2."\r\n".'Phone: '. $phone."\r\n".'Country: '. $country."\r\n".'Message: '.wordwrap($message, 70);
    $data = mail($email, $subject, $message, $headers);
}
?>


    <?php include "inc/footer.php" ?>