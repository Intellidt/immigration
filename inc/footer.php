<!-- Section 4 -->
<?php if (basename($_SERVER['PHP_SELF']) != "life_in_canada.php" && basename($_SERVER['PHP_SELF']) != "thank_you.php") { ?>
    <section class="section-13 logo-back">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="text-block-section d-flex align-items-center">
                        <h5>Book a free consultation with us. We will help make the process quicker, more efficient, and
                            stress-free for you.
                        </h5>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="text-block-section d-flex align-items-center">
                        <a href="contact.php" class="contact-link mt-20">Free Consultation</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

<!-- Footer -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <p class="footer-company">Intelli Management Consulting Corp.</p>
                <p class="footer-address">130-7080 River Road, Richmond, BC, V6X 1X5 Canada
                </p>
            </div>
            <div class="col-md-5">
                <h5 class="footer-head">Call us: +1 (778) 297-7108</h5>
                <h6 class="footer-alegada">Intelli Management Consulting Corp. © <?= date("Y"); ?> - All rights
                    reserved.</h6>
            </div>
        </div>
    </div>
</footer>
</div>

<?php }else{
    ?>
</div>

<footer>
    <div class="container">
                <h6 class="footer-alegada text-right">Intelli Management Consulting Corp. © <?= date("Y"); ?> - All rights
                    reserved.</h6>
    </div>
</footer>
<?php
} ?>
<!-- ++++++++++++-->
<!-- MAIN SCRIPTS-->
<!-- ++++++++++++-->
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<!-- Bootstrap-->
<script src="libs/bootstrap/bootstrap.js"></script>
<!-- Slider-->
<script src="libs/slider-pro/jquery.sliderPro.min.js"></script>
<script src="libs/owl-carousel/owl.carousel.min.js"></script>
<script src="libs/bxslider/jquery.bxslider.min.js"></script>
<!-- Pop-up window-->
<script src="libs/magnific-popup/jquery.magnific-popup.min.js"></script>
<!-- Mail scripts-->
<!-- Filter and sorting images-->
<script src="https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.js"></script>
<script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>
<!-- Animations-->
<script src="libs/scrollreveal/scrollreveal.min.js"></script>
<script src="libs/animate/wow.min.js"></script>
<script src="libs/animate/jquery.shuffleLetters.js"></script>
<script src="libs/animate/jquery.scrollme.min.js"></script>
<!-- Main slider-->
<script src="libs/slider-pro/jquery.sliderPro.min.js"></script>
<!-- User customization-->
<script src="js/custom.js"></script>

<script>
    $('.dropdown-menu a.dropdown-toggle').on('click', function (e) {
        if (!$(this).next().hasClass('show')) {
            $(this).parents('.dropdown-menu').first().find('.show').removeClass('show');
        }
        var $subMenu = $(this).next('.dropdown-menu');
        $subMenu.toggleClass('show');


        $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function (e) {
            $('.dropdown-submenu .show').removeClass('show');
        });


        return false;
    });

    $(document).ready(function () {
        // get the #section from the URL
        var hash = window.location.hash;
        // console.log(hash);
        $(hash).click();
    });

    $(".register-button").on('click', function () {
        $('html, body').animate({
            scrollTop: $("#webinar-form-section").offset().top
        }, 2000);
    })

    $("#webinar-register-form,#webinar-register-form-mobile").on("submit", function (e) {
        $(".alert-success,.alert-danger").addClass("d-none");
        e.preventDefault();
        var form_data = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "inc/functions.php",
            data: form_data,
            success: function (data) {
                if(data == "success"){
                    $(".registration-form-section,.alert-danger").addClass("d-none");
                    $(".alert-success").removeClass("d-none");
                }else if(data == "error"){
                    $(".alert-success").addClass("d-none");
                    $(".alert-danger").removeClass("d-none");
                }
            }
        });
    })

</script>

</body>
</html>