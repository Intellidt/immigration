<?php
include "constants.php";
if ($_POST["email"] != "") {

    $check_connection = mysqli_connect(DB_HOST, DB_USER, DB_PWD, DB_DATABASE);
    if ($check_connection) {
        $query = "SELECT `email_id` FROM registration WHERE (`email_id` = '".$_POST["email"]."' AND `is_verified` = 1 )";
        if (mysqli_query($check_connection, $query)) {
            echo "registered";
            exit();
        }
    }
    $subject = "[Life in Canada] Webinar Attendance Verification Required";
    $from_email = "Intelli Management Consultation <contact@intelliconsultation.com>";
    $to_email = $_POST["email"];
    $current_time = date("Y-m-d H:i:s");
    $first_name = ucfirst($_POST['fname']);
    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    $headers .= 'From: '.$from_email."\r\n". 'Reply-To: '.$from_email."\r\n" . 'X-Mailer: PHP/' . phpversion();
    $redirect_link = "";
    if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on')
        $redirect_link = "https";
    else
        $redirect_link = "http";
    $redirect_link .= "://";
    $redirect_link .= $_SERVER['HTTP_HOST'];
    $redirect_link .= "/immigration/thank_you.php?rid=".base64_encode($to_email."_".$first_name."_". $current_time);

    $message = "<p><br/>Hello ".$first_name.",<br/><br/>
                Thank you for registering for the \"Life in Canada\" webinar.<br/><br/>
                As there are limited spots, please click the link below to secure your spot for the event.<br/>
                <br/>".$redirect_link."<br/><br/>
                Should you have any questions or concerns please contact contact@intelliconsultation.com<br/><br/>
                Please note: This link is unique to the email recipient and should not be shared with others
                </p>
                <br/>
                <br/>
                This email was sent by:<br/><br/>
                Intelli Management Consultation Corp.<br/>
                200-3071 No. 5 Rd, Richmond, BC, V6X 2T4<br/>
                Tel: 778-297-7108<br/>
                <img src='http://intelligrp.com/immigration/images/logo1.png'>
                <br/><br/><br/><br/>
                <p style='font-size: 10px;'>
                The contents of this communication including any attachment(s) are confidential and may be privileged. If you are not the intended recipient (or are not receiving this communication on behalf of the intended recipient), please notify that sender immediately and delete or destroy this communication without reading it, and without make, forwarding, or retaining any copy or record of it or its contents. Thank you.
                </p>
                
                ";
    $data = mail($to_email, $subject, $message, $headers);
    if ($data) {
        $connection = mysqli_connect(DB_HOST, DB_USER, DB_PWD, DB_DATABASE);
        if ($connection) {
            $sql = "INSERT INTO registration (first_name, last_name, email_id,created_at) VALUES ('" . $_POST["fname"] . "', '" . $_POST["lname"] . "', '" . $to_email . "','" . $current_time . "')";
            if (mysqli_query($connection, $sql)) {
                echo "success";
            } else {
                echo "error";
            }
            mysqli_close($connection);
        }
        else{
            echo "error";
        }
    } else {
        echo "error";
    }
}