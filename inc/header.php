<!DOCTYPE html>
<html>
<!--<link href="style.css" rel='stylesheet'>-->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta content="telephone=no" name="format-detection" />
    <meta name="HandheldFriendly" content="true" />
    <link rel="stylesheet" href="css/master.css" />
    <link rel="shortcut icon" type="image/png" href="images/favicon.png"/>
    <title>Intelli Consultation</title>
</head>
<body>
<!-- Loader-->
<?php if(basename($_SERVER['PHP_SELF']) != "thank_you.php") { ?>
<div id="page-preloader"><span class="spinner border-t_second_b border-t_prim_a"></span></div>
<?php } ?>
<!-- Loader end-->
<div class="l-theme">

    <div class="search-block-hidden">
        <form class="search-global">
            <input class="search-global__input" type="text" placeholder="Type to search" autocomplete="off" name="s" value="">
            <button class="search-global__btn"><i class="icon stroke icon-Search"></i>
            </button>
            <div class="search-global__note">Begin typing your search above and press return to search.</div>
        </form>
        <div class="close-search"><i class="fas fa-times"></i></div>
    </div>
    <header class="header-top-absolute">
        <nav class="navbar navbar-expand-lg navbar-dark justify-content-between">
            <a class="navbar-brand d-flex" href="index.php"><img src="images/logo.png" alt="Intelli Management Consulting"></a>
            <div class="d-flex ">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"  aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <?php if (basename($_SERVER['PHP_SELF']) != "life_in_canada.php" && basename($_SERVER['PHP_SELF']) != "thank_you.php") {?>
                <ul class="nav">

                    <li class="close-nav"><i class="fas fa-times"></i></li>
                    <li class="nav-item <?php echo (basename($_SERVER['PHP_SELF']) == "index.php" ? "active" : "");?>">
                        <a class="nav-link " href="index.php">Home<span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item <?php echo (basename($_SERVER['PHP_SELF']) == "about.php" ? "active" : "");?>">
                        <a class="nav-link" href="about.php">About Us</a>
                    </li>
                    <li class="nav-item <?php echo (basename($_SERVER['PHP_SELF']) == "canada.php" ? "active" : "");?>">
                        <a class="nav-link" href="canada.php">About Canada</a>
                    </li>
                    <li class="nav-item dropdown <?php echo (basename($_SERVER['PHP_SELF']) == "services_express_entry.php" ? "active" : basename($_SERVER['PHP_SELF']) == "services_lmia.php" ? "active": basename($_SERVER['PHP_SELF']) == "services_caregiver.php" ? "active":basename($_SERVER['PHP_SELF']) == "services_work_permit.php" ? "active":basename($_SERVER['PHP_SELF']) == "services_study_permit.php" ? "active":basename($_SERVER['PHP_SELF']) == "services_visitor_permit.php" ? "active":basename($_SERVER['PHP_SELF']) == "services_family_sponsor.php" ? "active":basename($_SERVER['PHP_SELF']) == "services_startup_visa.php" ? "active":basename($_SERVER['PHP_SELF']) == "services_pnp.php" ? "active":basename($_SERVER['PHP_SELF']) == "services_pr.php" ? "active":basename($_SERVER['PHP_SELF']) == "services_citizenship.php" ? "active":"");?>">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Our Services</a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <li class="dropdown-submenu">
                                <a class="dropdown-item" href="services_express_entry.php">Express Entry</a>
                            </li>
                            <li class="dropdown-submenu">
                                <a class="dropdown-item" href="services_lmia.php">LMIA</a>
                            </li>

                            <li class="dropdown-submenu">
                                <a class="dropdown-item" href="services_family_sponsor.php">Family Sponsorship</a>
                            </li>
                            <li class="dropdown-submenu">
                                <a class="dropdown-item" href="services_startup_visa.php">Start Up Visa</a>
                            </li>
                            <li class="dropdown-submenu">
                                <a class="dropdown-item" href="services_pnp.php">PNP</a>
                            </li>
                            <li class="dropdown-submenu">
                                <a class="dropdown-item" href="services_caregiver.php">Caregiver</a>
                            </li>
                            <li class="dropdown-submenu">
                                <a class="dropdown-item" href="services_work_permit.php">Work Permit</a>
                            </li>
                            <li class="dropdown-submenu">
                                <a class="dropdown-item" href="services_study_permit.php">Study Permit</a>
                            </li>
                            <li class="dropdown-submenu">
                                <a class="dropdown-item" href="services_visitor_permit.php">Visitor Visa</a>
                            </li>
                            <li class="dropdown-submenu">
                                <a class="dropdown-item" href="services_pr.php">PR Card</a>
                            </li>
                            <li class="dropdown-submenu">
                                <a class="dropdown-item" href="services_citizenship.php">Citizenship Application</a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item <?php echo (basename($_SERVER['PHP_SELF']) == "contact.php" ? "active" : "");?>">
                        <a class="nav-link" href="contact.php">Contact Us</a>
                    </li>
                </ul>

                <?php
                }
                ?>

            </div>
        </nav>
    </header>

    <!-- end .header-->
