<?php include "inc/header.php" ?>
<section style="background: #071141;">
    <div class="banner-back">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="title-text empty-back">
                    <!--<h5>Business Immigration Programs</h5>-->
                    <h2>Privacy Policy</h2>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end Blog header-->
<!--  Article -->
<section class="section-2">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="content-wrap practice-single">
                    <div class="content-text">
                        <p class="par-p">
                            This Privacy Policy applies to: intelliconsultation.com<br/>
                            Last update: June 29, 2020<br/><br/>

                            The respect of your private life is of utmost importance for Intelli Management Consulting Corp., who is responsible for this website.
                            <br/><br/>
                            This Privacy Policy aims to lay out:
                            <br/><br/>
                            The way your personal information is collected and processed. “Personal Information” means any information that could identify you, such as your name, your mailing address, your email address, your location and your IP address;
                            <br/><br/>
                            your rights regarding your personal information;
                            <br/><br/>
                            Who is responsible for the processing of the collected and processed information;
                            <br/><br/>
                            To whom the information is transmitted;
                            <br/><br/>
                            If applicable, the website’s policy regarding cookies.
                        </p>
                        <p class="par-p">
                            This Privacy Policy compliment the Terms and Conditions that you may find at the following address:
                        </p>
                        <div class="par-list">
                            <ol class="ordered-list">
                                <li>
                                    <p class="par-p color-blue">
                                        COLLECTION OF PERSONAL INFORMATION
                                    </p>
                                    <p class="par-p mb-0">
                                    We collect the following personal information:
                                    </p>
                                    <ul class="list-left">
                                        <li class="color-black">
                                            Last Name
                                        </li>
                                        <li class="color-black">
                                            First Name
                                        </li>
                                        <li class="color-black">
                                            Email Address
                                        </li>
                                        <li class="color-black">
                                            Phone and/or fax number
                                        </li>
                                        <li class="color-black">
                                            Gender
                                        </li>
                                        <li class="color-black">
                                            Education
                                        </li>
                                        <li class="color-black">
                                            Occupation
                                        </li>
                                        <li class="color-black">
                                            Family/marital status
                                        </li>
                                    </ul>
                                    <p class="par-p mt-3 mb-0">
                                    Below is a list of reasons why we collect data, these may include:
                                    </p>
                                    <ul class="list-left">
                                        <li class="color-black">
                                            Marketing communications which you have requested on our sign up pages or from your updated profile, this includes information about Intelli Management Consulting:
                                            <ul class="list-left">
                                                <li class="color-black">
                                                    Products and services;
                                                </li>
                                                <li class="color-black">
                                                    Events and promotions;
                                                </li>
                                                <li class="color-black">
                                                    Activities;
                                                </li>
                                                <li class="color-black">
                                                    Promotions of our associated partners’ services. Please note these marketing communications are subscription based and require your consent; and/or
                                                </li>
                                                <li class="color-black">
                                                    Products and services that you have purchased from us
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="color-black">
                                            Perform direct sales based on a reply to a ‘Contact me’ or other web forms that were completed on our website or email;
                                        </li>
                                        <li class="color-black">
                                            Perform contractual obligations like:
                                            <ul class="list-left">
                                                <li class="color-black">
                                                    Order confirmations;
                                                </li>
                                                <li class="color-black">
                                                    Invoices;
                                                </li>
                                                <li class="color-black">
                                                    Reminders;
                                                </li>
                                                <li class="color-black">
                                                    Notifications about any disruptions to our services; and/or
                                                </li>
                                                <li class="color-black">
                                                    Process a job application
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="color-black">
                                            Follow ups on incoming requests from:
                                            <ul class="list-left">
                                                <li class="color-black">
                                                    Customer support;
                                                </li>
                                                <li class="color-black">
                                                    Emails;
                                                </li>
                                                <li class="color-black">
                                                    Chats; and/or
                                                </li>
                                                <li class="color-black">
                                                    Phone calls
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="color-black">
                                            Direct marketing;
                                        </li>
                                        <li class="color-black">
                                            Help resolve any disputes, collecting fees, and carrying out troubleshooting activities;
                                        </li>
                                        <li class="color-black">
                                            Improve our services and website; and
                                        </li>
                                        <li class="color-black">
                                            Conduct surveys for the purpose of market research
                                        </li>
                                    </ul>
                                    <p class="par-p mt-3 mb-0">
                                    We also collect data about:
                                    </p>
                                    <ul class="list-left">
                                        <li class="color-black">
                                            Suppliers;
                                        </li>
                                        <li class="color-black">
                                            Subcontractors;
                                        </li>
                                        <li class="color-black">
                                            Sponsors;
                                        </li>
                                        <li class="color-black">
                                            Partners; and
                                        </li>
                                        <li class="color-black">
                                            Persons seeking a job or working in our company
                                        </li>
                                    </ul>
                                    <p class="par-p mt-3 mb-0">
                                    The personal information we collect is collected through the collection methods described in the following section.
                                    </p>
                                </li>
                                <li>
                                    <p class="par-p color-blue mt-3">
                                        FORMS AND METHODS OF COLLECTION
                                    </p>
                                    <p class="par-p mt-3 mb-0">
                                    Your personal information is collected through the following methods:
                                    </p>
                                    <ul class="list-left">
                                        <li class="color-black">
                                            Survey form
                                        </li>
                                        <li class="color-black">
                                            Webinar
                                        </li>
                                        <li class="color-black">
                                            Email
                                        </li>
                                    </ul>
                                    <p class="par-p mt-3 mb-0">
                                    We use the collected data for the following purpose:
                                    </p>
                                    <ul class="list-left">
                                        <li class="color-black">
                                            Consultation Services
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <p class="par-p color-blue mt-3">
                                        INTERACTIVITY
                                    </p>
                                    <p class="par-p mt-3 mb-0">
                                    Your personal information is also collected through the interactivity between you and the website. This personal information is collected through the following methods:
                                    </p>
                                    <ul class="list-left">
                                        <li class="color-black">
                                            Correspondence
                                        </li>
                                        <li class="color-black">
                                            Event Sign-up
                                        </li>
                                    </ul>
                                    <p class="par-p mt-3 mb-0">
                                    We use the personal information thus collected for the following purposes:
                                    </p>
                                    <ul class="list-left">
                                        <li class="color-black">
                                            Contact
                                        </li>
                                        <li class="color-black">
                                            Products and Services
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <p class="par-p color-blue mt-3">
                                        SHARING OF PERSONAL INFORMATION
                                    </p>
                                    <p class="par-p mt-3 mb-0">
                                    We are committed to not selling to third parties or otherwise commercialize the personal information we collect. However, we may share this information with the third parties for the following reason:
                                    </p>
                                    <ul class="list-left">
                                        <li class="color-black">
                                            Products and services referral
                                        </li>
                                    </ul>
                                    <p class="par-p mt-3 mb-0">
                                    If you do wish that we not share your personal information with third parties, you may object at time of collection or at any time thereafter, as described in the “Right of objection and of withdrawal” section.
                                    </p>
                                </li>
                                <li>
                                    <p class="par-p color-blue mt-3">
                                        STORAGE PERIOD OF PERSONAL INFORMATION
                                    </p>
                                    <p class="par-p mt-3 mb-0">
                                    The Controller will keep in its computer systems, in reasonable security conditions, the entirety of the personal information collected for the following duration: 60 months.
                                    <br/>
                                    How long your data is stored depends on:
                                    </p>
                                    <ul class="list-left">
                                        <li class="color-black">
                                            How long it takes to answer your queries;
                                        </li>
                                        <li class="color-black">
                                            Resolve any issues;
                                        </li>
                                        <li class="color-black">
                                            Comply with legal requirements under applicable law systems; and/or
                                        </li>
                                        <li class="color-black">
                                            Resolve any legal claims and/or complaints
                                        </li>

                                    </ul>
                                    <p class="par-p mt-3 mb-0">
                                    Your data will be stored for a reasonable amount of time after your last interaction with our company. When your personal data is no longer required, it shall be deleted in a secure manner. If we process your data for statistical purposes, all data provided will be anonymous
                                    </p>
                                </li>
                                <li>
                                    <p class="par-p color-blue mt-3">
                                        RIGHT OF ACCESS, OF RESTRICTION AND OF REMOVAL
                                    </p>
                                    <p class="par-p mt-3 mb-0">
                                    You have the right to consult, update, modify or request the removal of information about you by following the procedure described hereinafter:
                                    </p>
                                </li>
                                <li>
                                    <p class="par-p color-blue mt-3">
                                        CHANGES TO OUR PRIVACY POLICY
                                    </p>
                                    <p class="par-p mt-3 mb-0">
                                    Our Privacy Policy may be viewed at all times at the following address:
                                    <br/>
                                    (Insert url link)
                                    <br/>
                                    We reserve the right to modify our Privacy Policy in order to guarantee its compliance with the applicable law.
                                    <br/>
                                    You are therefore invited to regularly consult our privacy Policy to be informed of the latest changes.
                                    </p>
                                </li>
                                <li>
                                    <p class="par-p color-blue mt-3">
                                        ACCEPTANCE OF OUR POLICY
                                    </p>
                                    <p class="par-p mt-3 mb-0">
                                    By using our Website, you certify that you have read and understood this Privacy Policy and accept its conditions, more specifically conditions relating to collection and processing of personal information.
                                    </p>
                                </li>
                                <li>
                                    <p class="par-p color-blue mt-3">
                                        APPLICABLE LAW
                                    </p>
                                    <p class="par-p mt-3 mb-0">
                                    We are committed to respect the legislative provisions as specified in:
                                    <br/>
                                    Personal Information Protection and Electronic Documents Act, SC 2000, c 5; and/or Act Respecting the Protection of Personal Information in the Private Sector, CQLR cP-39-.1.
                                    </p>
                                </li>
                            </ol>
                        </div>
                        <h4 class="par-head-h4">
                            Disclaimers for emails
                        </h4>
                        <p class="par-p mb-0 color-blue">
                        Confidentiality
                        </p>
                        <p class="par-p">
                        All content within any information communicated between you and our company via email is confidential and intended only for the recipient. Any sharing of this content with a third party must be first approved by the recipient with their express consent. If you receive an email from our company by mistake, please reply to us to confirm this error. Then kindly delete the email from your inbox.
                        </p>
                        <p class="par-p mb-0 color-blue">
                        Privacy
                        </p>
                        <p class="par-p">
                        You will only receive emails communication through our company if you have expressly specified to. All information that is communicated between you the user and our company will be kept confidential, unless said information is required for the processing of your case. Should your information need to be shared with third parties, your express consent is required in order to continue with your case. If you did not expressly sign up to be part of our mailing list, you can contact us at contact@intelliconsultation.com.
                        </p>
                        <p class="par-p mb-0 color-blue">
                        Security
                        </p>
                        <p class="par-p">
                        We always aim to ensure that your emails and their attachments are free from viruses. Unfortunately we cannot always ensure that messages are error or virus free, have not been corrupted and/or intercepted by third parties. Please always ensure that any emails or attachments you receive are scanned with appropriate software. Please note that we do not accept any liability for damages caused by, or inflicted through the sending or receiving of information between the user and our company. Should a breach in your sensitive information occur, you will be contacted within 72 hours of the event by a communication channel you have approved.
                        </p>
                        <p class="par-p mb-0 color-blue">
                        Contracts
                        </p>
                        <p class="par-p">
                        Any information sent between you the user and our company is not legally binding, until a legal document has been signed. Users can agree or decline our services based on our Term and Conditions and Privacy Policy. No employee within our company has the right to offer you a legally binding contract without the express permission of their supervising manager.
                        </p>
                        <p class="par-p mb-0 color-blue">
                        Unsubscribe
                        </p>
                        <p class="par-p">
                        If you wish to unsubscribe from our site, you contact us at contact@intelliconsultation.com to discontinue your subscription. All your personal information in our database will be stored for a set period of time, before it is permanently deleted from our system.
                        </p>
                        <p class="par-p mb-0 color-blue">
                        Right to restrict processing
                        </p>
                        <p class="par-p">
                        At any time, from signing up with our website, you have the right to restrict your processing with our company. If you have any trouble with this aspect, please immediately contact us at contact@intelliconsultation.com
                        </p>
                        <p class="par-p">
                        Only with your express consent can our Account Managers or company contact you via:
                        </p>
                        <ul class="list-left">
                            <li class="color-black">
                                Email;
                            </li>
                            <li class="color-black">
                                Phone call; and/or
                            </li>
                            <li class="color-black">
                                SMS
                            </li>
                        </ul>
                        <p class="par-p">
                        Any information you store with us will remain in our data base for a set period of time, before it is deleted. Your personal information will not be used at any time during this period.
                        </p>
                        <p class="par-p mb-0 color-blue">
                        Right to object
                        </p>
                        <p class="par-p">
                        This also includes the right to restrict processing as a potential client of intelliconsultation.com. Your information will not be used for direct marketing.
                        </p>
                    </div>

                </div>
            </div>
            <!-- end col-md-12 -->
        </div>
    </div>
</section>


<?php include "inc/footer.php" ?>
