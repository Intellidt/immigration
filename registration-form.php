<div class="alert alert-success d-none mx-2" role="alert">
    Thank you for your registration. Please check your email to reserve your spot.
</div>
<div class="contact-form-section registration-form-section">
    <h4 class="font-head-it mb-3">Please fill in your full name and email below to register for the webinar</h4>
    <p class="mb-4">Due to limited seating, please check your email for a verification link after
        registering to reserve your spot and confirm your attendance</p>

    <form id="<?php echo $id; ?>" method="post">
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-danger d-none" role="alert">
                    Something went wrong!
                </div>
            </div>
        </div>
        <div class="row ">
            <div class="col-md-12">
                <input class="form-input" placeholder="First Name" name="fname"
                       type="text" required></div>
        </div>
        <div class="row mt-20 ">
            <div class="col-md-12"><input class="form-input" placeholder="Last Name" name="lname"
                                          type="text" required></div>
        </div>
        <div class="row mt-20 ">
            <div class="col-md-12"><input class="form-input" placeholder="Email" name="email"
                                          type="email" required></div>
        </div>
        <div class="row mt-20 ">
            <div class="col-md-12">
                <input type="checkbox" name="confirm_checkbox" required/> I agree to the Intelli Management Consulting Corp. <a href="terms_and_conditions.php">Terms and Conditions</a> and <a href="privacy_policy.php">Privacy Policy</a>
            </div>
        </div>
        <div class="row mt-1 ">
            <div class="col-md-12">
                <input class="float-right" value="Register Now" name="send" type="submit">
            </div>
        </div>
    </form>
</div>