<?php include "inc/header.php" ?>

<section class="caregiver-back">
    <div class="banner-back">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="title-text">
                    <!--<h5>Options available for Permanent Residence in Canada</h5>-->
                    <h2>Caregiver</h2>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end Blog header-->
<!--  Article -->
<section class="section-2">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-12 col-sm-12">
                <div class="content-wrap practice-single">
                    <div class="content-text">
                        <h3 class="font-head-it">Immigrate by providing care for children, the elderly or those with medical needs</h3>
                                <h4 class="par-head-h4">Requirements:</h4>
                                <div class="par-list">
                                    <ul class="list-left">
                                        <li>Language test results, at least benchmark level 5</li>
                                        <li>Past experience or training</li>
                                        <li>Education: completed post-secondary education credential of at least 1 year in Canada or equivalent</li>
                                    </ul>
                                </div>
                                <h4 class="par-head-h4">Limitation:</h4>
                                <p class="par-p">
                                    Only process 5,500 applications each year (2,750 Home Child Care Provider Pilot; 2,750 Home Support Worker Pilot)
                                </p>
                                <h4 class="par-head-h4">Time:</h4>
                                <p class="par-p">
                                    24 months of work experience to qualify for PR
                                </p>
                                <h4 class="par-head-h4">Advantages / Outcome:</h4>
                                <div class="par-list">
                                    <ul class="list-left">
                                        <li>If there is no qualifying work experience, applicants can apply for Permanent Residence (PR) through Home Child Care Provider Pilot or Home Support Worker Pilot as long as other eligibility requirements are met </li>
                                        <li>Family members are eligible to come with applicant and can obtain work or study permits</li>
                                        <li>Access to Universal Healthcare</li>
                                    </ul>
                                </div>
                    </div>

                </div>
            </div>
            <!-- end col-md-9 -->
            <!--  col-md-3 -->
            <div class="col-lg-3 col-md-12 col-sm-12 ">
                <div class="blog-wrap-right">
                    <div class="banner">
                        <h4>Solutions based on your assessment results and needs. Talk to our team of friendly professionals now and tailor a solution suited for you.</h4>
                        <a class="contact-link"  href="contact.php">Free Consultation</a>
                    </div>
                    <div class="category blog-social">
                        <h4 class="font-head">Other Services</h4>
                        <div class="line-red-2"></div>
                        <ul>
                            <li><a href="services_express_entry.php">Express Entry</a></li>
                            <li><a href="services_lmia.php">LMIA</a></li>
                            <li><a href="services_pnp.php">PNP</a></li>
                            <li><a href="services_startup_visa.php">Start Up Visa</a></li>
                            <li><a href="services_work_permit.php">Work Permit</a></li>
                            <li><a href="services_study_permit.php">Study Permit</a></li>
                            <li><a href="visitorvisa.php">Visitor Visa</a></li>
                            <li><a href="services_family_sponsor.php">Family Sponsorship</a></li>
                            <li><a href="services_pr.php">PR Card</a></li>
                            <li><a href="services_citizenship.php">Citizenship Application</a></li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>


<?php include "inc/footer.php" ?>
