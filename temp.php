<?php include "inc/header.php" ?>
    <section class="life-in-canada">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title-text">
                        <h2>Life in Canada</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="contact-section webinar-contact-form">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div>
                        <h4 class="par-head-h4 mt-0">Explore the World from Home : <br/>Getting to Know One of the Best Places on Earth</h4>
                        <p class="par-p">A Live Webinar event offering a glimpse of what life is like for people living in Canada.
                            Specialist guest speakers from various industries will join this exclusive event to provide
                            informative insights with regards to settling down in Canada, specifically the province of
                            British Columbia. Time permitted, attendees are welcome to raise questions of our
                            specialists towards the end of this event.</p>
                    </div>
                    <div class="w-100" id="mobile-register">
                        <input class="register-button" value="Sign Up" name="send"
                               type="submit">
                    </div>
                    <div class="mt-5">
                        <h4 class="font-head-it pr-2">Topics included</h4>
                        <div class="par-list">
                            <ul class="list-left">
                                <li>Canadian Culture, Traditions, & Advantages</li>
                                <li>Living in Canada</li>
                                <li>Education System</li>
                                <li>Banking Services & Guidelines</li>
                                <li>Accounting Services</li>
                                <li>Legal Services</li>
                                <li>Immigration Services</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-6" id="register-form">
                    <?php
                    $id = "webinar-register-form";
                    include "registration-form.php"
                    ?>
                </div>
            </div>
        </div>
    </section>
    <section class="organized-by">
        <div class="container">
            <div class="row row-eq-height">
                <div class="col-md-4 organized-by-section float-left">
                    <p class="organized-by-title">Organized by</p>
                    <div class="mt-3">
                        <img src="images/logo1.png" alt="Logo"/>
                    </div>
                </div>
                <div class="col-md-4 organized-by-section float-left">
                    <p class="organized-by-title">Venue</p>
                    <div class="venue-name">ZOOM VIDEO CONFERENCING</></div>
                <p class="mt-3 mb-1"><u>Canada</u></p>
                <div class="">Friday, 17 July 2020</div>
                <div class="">8:00 – 9:00 pm (PST)</div>
                <p class="mt-3 mb-1"><u>Hong Kong</u></p>
                <div class="">Saturday, 18 July 2020</div>
                <div class="">11:00 am – 12:00 pm (HKT)</div>
            </div>
            <div class="col-md-4 float-left par-list">
                <p class="organized-by-title ">Guest speakers</p>
                <ul class="mb-0 list-left organized-by-list">
                    <li>Canadian Bank Representative</li>
                    <li>Richmond City Councillor</li>
                    <li>Immigration Consultant</li>
                    <li>Canadian Educator</li>
                    <li>Canadian Lawyer</li>
                    <li>Canadian Business Association Representative</li>
                </ul>
            </div>
            <div class="clearfix"></div>
        </div>
        </div>
        </div>
    </section>
    <section id="webinar-form-section" class="mt-5 mb-5">
        <div class="container">
            <?php
            $id = "webinar-register-form-mobile";
            include "registration-form.php"
            ?>
        </div>
    </section>
<?php include "inc/footer.php" ?>