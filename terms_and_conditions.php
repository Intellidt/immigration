<?php include "inc/header.php" ?>
<section style="background: #071141;">
    <div class="banner-back">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="title-text empty-back">
                    <!--<h5>Business Immigration Programs</h5>-->
                    <h2>Terms and Conditions</h2>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end Blog header-->
<!--  Article -->
<section class="section-2">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="content-wrap practice-single">
                    <div class="content-text">
                        <p class="par-p">
                            Last revision July 3, 2020<br/><br/>
                            PLEASE READ THESE TERM OF USE CAREFULLY BEFORE USING THIS WEBSITE<br/><br/>

                            The following Terms and Conditions govern and apply to your use of or reliance upon this website maintained by Intelli Management Consulting Corp. (the “Website”).
                            <br/><br/>
                            Your access or use of the Website indicates that you have read, understand and agree to be bound by these Terms and Conditions and any other applicable laws, statues and/or regulations. We may change these Terms and Conditions at any time without notice, effective upon its posting to the Website. Your continued use of the Website will be considered your acceptance to the revised Terms and Conditions.
                        </p>
                        <div class="par-list">
                            <ol class="ordered-list">
                                <li>
                                    <p class="par-p color-blue">
                                        AGE RESTRICTION
                                    </p>

                                    <p class="par-p">
                                        You must be at least eighteen (18) years of age to use this Website or any services contained herein. Your access or use of this Website indicates your representation that you are at least eighteen (18) years of age. We assume no responsibility or liability for any misrepresentation of your age.
                                    </p>
                                </li>
                                <li>
                                    <p class="par-p color-blue">
                                        INTELLECTUAL PROPERTY
                                    </p>
                                    <p class="par-p">
                                        We may provide you with certain information as a result of your use of the Website including, but not limited to, documentations, data, or information developed by us, and other materials which may assist in the use of the Website or Services (“Company Materials”). The Company Materials may not be used for any other purpose than the use of this Website and the services offered on the Website. Nothing in these Terms of Use may be interpreted as granting any license of intellectual property rights to you.
                                    </p>
                                </li>
                                <li>
                                    <p class="par-p color-blue">
                                        USE OF COMPANY MATERIALS
                                    </p>
                                    <p class="par-p">
                                        All Intellectual property on the Website is owned by us or our licensors, which includes materials protected by copyright, trademark, or patent laws. All trademarks, service marks and trade names are owned, registered and/or licensed by us. All content on the Website, including but not limited to text, software, code, designs, graphics, photos, sounds, music, applications, interactive features and all other content is a collective work under Canadian and other copyright laws and is the proprietary property of Intelli Management Consulting Corp. (“the Company”); All rights reserved.
                                    </p>
                                </li>
                                <li>
                                    <p class="par-p color-blue">
                                        ACCEPTABLE USE
                                    </p>
                                    <p class="par-p mb-0">
                                    You agree not to use the Website for any unlawful purpose or any purpose prohibited under this clause. You agree not to use the Website in any way that could damage the Website, the services or the general business of Intelli Management Consulting Corp.
                                    <br/>
                                    You further agree not to use and/or access the Website:
                                    </p>
                                    <ul class="list-left">
                                        <li class="color-black">
                                            To harass, abuse, or threaten others or otherwise violate any person’s legal rights;
                                        </li>
                                        <li class="color-black">
                                            To violate any intellectual property rights of us on any third party;
                                        </li>
                                        <li class="color-black">
                                            To upload or otherwise disseminate any computer viruses or other software that may damage the property of another;
                                        </li>
                                        <li class="color-black">
                                            To perpetrate any fraud;
                                        </li>
                                        <li class="color-black">
                                            To engage in or create any unlawful gambling, sweepstakes, or pyramid scheme;
                                        </li>
                                        <li class="color-black">
                                            To engage in or create any unlawful gambling, sweepstakes, or pyramid scheme;
                                        </li>
                                        <li class="color-black">
                                            To publish or distribute any material that incites violence, hate or discrimination towards any group;
                                        </li>
                                        <li class="color-black">
                                            To unlawfully gather information about others
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <p class="par-p color-blue mt-3">
                                        PROTECTION OF PRIVACY
                                    </p>
                                    <p class="par-p">
                                    Through the use of the Website, you may provide us with certain information. By using the Website, you authorize us to use your information in Canada and any other country where we may operate.
                                    </p>
                                    <p class="par-p">
                                    We may receive information from external applications you use to access our Website, or we may receive information through various web technologies, such as cookies, log files, clear gifs, web beacons or others.
                                    </p>
                                    <p class="par-p">
                                    We use the information gathered from you to ensure your continued good experience on our website. We may also track certain of the passive information received to improve our marketing and analytics, and for this, we may work with third-party providers, including other marketers.
                                    </p>
                                    <p class="par-p">
                                    If you would like to disable our access to any passive information we received from the use of various technologies, you may choose to disable cookies in your web browser.
                                    </p>
                                </li>
                                <li>
                                    <p class="par-p color-blue">
                                        ASSUMPTION OF RISK
                                    </p>
                                    <p class="par-p">
                                    The Website is provided for communication purposes only. You acknowledge and agree that any information posted on our Website is not intended to be legal advice, medical advice, or financial advice, and no fiduciary relationship has been created between you and Intelli Management Consulting Corp. You further agree that any personal information you submit to us over the Website is at your own risk. We do not assume responsibility or liability for any advice or other information given on the Website.
                                    </p>
                                </li>
                                <li>
                                    <p class="par-p color-blue">
                                        REVERSE ENGINEERING & SECURITY
                                    </p>
                                    <p class="par-p mb-0">
                                    You may not undertake any of the following actions:
                                    </p>
                                    <ul class="list-left">
                                        <li class="color-black">
                                            Reverse engineer, or attempt to reverse engineer or disassemble any code or software from or on the Website;
                                        </li>
                                        <li class="color-black">
                                            Violate the security of the Website through any unauthorized access, circumvention of encryption or other security tools, data mining or interference to any host, user of network.
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <p class="par-p color-blue mt-3">
                                        INDEMIFICATION
                                    </p>
                                    <p class="par-p">
                                    You defend and indemnify Intelli Management Consulting Corp. and any of its affiliates and hold us harmless against any and all legal claims and demands, including reasonable attorney’s fees, which may arise from or relate to your use or misuse of the Website, your breach of these Terms and Conditions, or your conduct or actions. We will select our own legal counsel and may participate in our own defence, if we wish to do so.
                                    </p>
                                </li>
                                <li>
                                    <p class="par-p color-blue">
                                        SERVICE INTERRUPTIONS
                                    </p>
                                    <p class="par-p">
                                        We may need to interrupt your access to the Website to perform maintenance or emergency services on a scheduled or unscheduled basis. You agree that your access to the Website may be affected by unanticipated or unscheduled downtime, for any reason, but that we will have no liability for any damage or loss caused as a result of such downtime.
                                    </p>
                                </li>
                                <li>
                                    <p class="par-p color-blue">
                                        NO WARRANTIES
                                    </p>
                                    <p class="par-p">
                                        Your use of the Website is at your sole and exclusive risk and any services provided by us are on an “as is” basis. We disclaim any and all express or implied warranties of any kind, including, but not limited to the implied warranty of fitness for a particular purpose and the implied warranty of merchantability. We make no warranties that the Website will meet your needs or that the Website will be uninterrupted, error-free, or secure. We also make no warranties as to the reliability or accuracy of any information on the Website or obtained through the Services. Any damage that may occur to you, through your computer system, or as a result of loss of your data from your use of the Website is your sole responsibility and we are not liable for any such damage or loss.
                                    </p>
                                </li>
                                <li>
                                    <p class="par-p color-blue">
                                        PRIVACY
                                    </p>
                                    <p class="par-p">
                                        In addition to these Terms and Conditions, this Website has a Privacy Policy that describes how your personal data is collected and how cookies are used on the Website. For more information, you may refer to our Privacy Policy, available on the Website.
                                    <br/>
                                        By using or browsing this Website, you also acknowledge that you have read our Privacy Policy.
                                    </p>
                                </li>
                                <li>
                                    <p class="par-p color-blue">
                                        LIMITATION ON LIABILITY
                                    </p>
                                    <p class="par-p">
                                        We are not liable for any damages that may occur to you as a result of your use of the Website, to the fullest extent permitted by law. The maximum liability of Intelli Management Consulting Corp. arising from your use of the Website is limited to the greater of one hundred ($100) Canadian Dollars of the amount you paid to Intelli Management Consulting Corp. in the last six (6) months. This applies to any and all claims by you, but not limited to, lost of profits or revenues, consequential or punitive damages, negligence, strict liability, fraud, or torts of any kind.
                                    </p>
                                </li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
            <!-- end col-md-12 -->
        </div>
    </div>
</section>


<?php include "inc/footer.php" ?>
