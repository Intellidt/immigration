<?php
include "inc/constants.php";
$success = 0;
$registration_id = $_GET["rid"];
if ($registration_id != "") {
    $connection = mysqli_connect(DB_HOST, DB_USER, DB_PWD, DB_DATABASE);
    if ($connection) {
        $get_string = explode("_", base64_decode($registration_id));
        if (count($get_string) == 3) {
            $query = "SELECT * from registration where email_id='" . $get_string[0] . "' AND created_at='" . $get_string[2] . "' AND is_verified = 0";
            if ($res = mysqli_query($connection, $query)) {
                while ($row = mysqli_fetch_array($res)) {
                    $update_query = "UPDATE registration set is_verified = 1 where email_id='" . $get_string[0] . "' AND created_at='" . $get_string[2] . "'";


                    if (mysqli_query($connection, $update_query)) {
                        $success = 1;
                        $subject = "[Life in Canada] Webinar Attendance Confirmation";
                        $from_email = "Intelli Management Consultation <contact@intelliconsultation.com>";
                        $to_email = $get_string[0];

                        $headers  = 'MIME-Version: 1.0' . "\r\n";
                        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                        $headers .= 'From: '.$from_email."\r\n". 'Reply-To: '.$from_email."\r\n" . 'X-Mailer: PHP/' . phpversion();

                        $message = "<p><br/>Hello ".$get_string[1].",<br/><br/>
                                    Thank you for registering for the \"Life in Canada\" Webinar, your spot has been reserved.<br/><br/>
                                    Please look out for the event details which will be sent to you one week prior to the event.<br/><br/>
                                    We look forward to sharing with you various insights to living life in Canada.<br/><br/>
                                    Should you have any questions or concerns please contact contact@intelliconsultation.com<br/><br/>
                                    </p>
                                    <br/>
                                    <br/>
                                    This email was sent by:<br/><br/>
                                    Intelli Management Consultation Corp.<br/>
                                    200-3071 No. 5 Rd, Richmond, BC, V6X 2T4<br/>
                                    Tel: 778-297-7108<br/>
                                    <img src='http://intelligrp.com/immigration/images/logo1.png'>
                                    <br/><br/><br/><br/>
                                    <p style='font-size: 10px;'>
                                    The contents of this communication including any attachment(s) are confidential and may be privileged. If you are not the intended recipient (or are not receiving this communication on behalf of the intended recipient), please notify that sender immediately and delete or destroy this communication without reading it, and without make, forwarding, or retaining any copy or record of it or its contents. Thank you.
                                    </p>
                                    
                                    ";

                        $data = mail($to_email, $subject, $message, $headers);
                    }
                }
            }
            mysqli_close($connection);
        }
    }
}
?>
<?php include "inc/header.php" ?>

<section class="life-in-canada">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="title-text">
                    <h2>Life in Canada</h2>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="contact-section webinar-contact-form">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <?php if ($success) { ?>
                    <div class="alert alert-success" role="alert">
                        Thank you for your registration. Your spot is now reserved.
                    </div>
                <?php }else{ ?>
                    <div class="alert alert-danger" role="alert">
                        Invalid link!!
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</section>
<?php include "inc/footer.php" ?>
